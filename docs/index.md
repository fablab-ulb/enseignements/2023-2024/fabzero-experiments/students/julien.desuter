## Foreword

Hello and welcome to my personal page for the FabLab class : [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

The goal of this personal website is to explain and show all the work done during this class. Doing so by documenting each step but also the errors and difficulties encountered along the way.

This website contains the documentation of each 5 assignments of the class:

1. Project management and documentation
2. Computer-Aided Design (CAD)
3. 3D printing
4. Electronic prototyping
5. Team dynamics and final project

## About me

So this is me ! I am Julien Desuter currently in my second year of master in Physics at ULB. Concerning my studies I am currently doing my master thesis on binary stars using data from the Gaia telescope and older literature regrouped in the SB9 catalog. I also did an internship this summer at the Royal Belgian Institute of Spacial Aeronomy where I had the opportunity to work on VenSpec-H, an instrument (IR high resolution spectrometer) that will be part of the EnVision mission launching early 2030's with the goal of studying the Venusian volcanic activity (Through, in the case of VenSpec-H, the variation of certain molecules in the atmosphere).

<img src="images/small_about_me.jpg" width="400">

Outside of my studies, I like to have fun with friends, do sports (running, swimming, diving, sailing and probably starting boxing as soon as I find free time) but the one sport that I can't love the most is skiing. I also love to learn and discover new things (that's one of the main reason why I chose this class). It is also why I love traveling so much ! Meeting new people and culture. My comfort zone is when I am out of what I know.

My biggest travel so far probably stays my year abroad in the US. I did my senior year of high school in Timberlake High School in Spirit Lake a very small town (about 2000 people) in the mountains of North Idaho. I was hosted by  Monica and Jim in their sixties and Seventh Day Adventist. The cultural shock was huge but it made the year even more interesting. I am always coming back from somewhere and thinking about my next destination.

Yeah that's pretty much it ! Other than that would be random stories or dark jokes...

## Contact:

- julien.desuter@ulb.be
- discord : Louxas#2565
- Instagram and messenger by my name
