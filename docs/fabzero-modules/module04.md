# 4. Electronic prototyping

This week assignment was about electronic and micro controller. The goal here was to familiarize ourself to a MCU board with input and output devices.

## Setup

At the beginning of the class, we were given a kit with a MCU, a few sensors and LEDs but also wires and resistances. The whole content of this kit can be found [here](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/hardware/kit/). The MCU that we were given was assembled on a module (YD-RP2040) which is an equivalent to the [Raspberry Pi PICO](https://www.raspberrypi.com/products/raspberry-pi-pico/) with some additional features (Neopixel LED and button).

Here is the module :

<img src="../images/mod4/mcu.png" width="500">

To control this micro controller board, we used [Arduino IDE](https://www.arduino.cc/en/software) that is a derivative of the C language used for electronic. While using this software, you need to select the board and port that you're using (as shown below)

<img src="../images/mod4/arduino.png" width="500">

But first, you need to install the board that we are using. To do so, go to file > preferences and add [this URL](https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json) to additional board manager URLs as shown on the screenshot below

<img src="../images/mod4/arduino2.png" width="500">

After that, find and install the board in your board manager like that :

<img src="../images/mod4/arduino3.png" width="200" height="250">

### Common problem

An issue that most of the class had at one point is the USB port not being recognized by our computer. To solve that, you just need to disconnect your module from your computer and then reconnect it while pushing on the "boot" button. This will reboot your MCU and it should work properly.

## Exo 1 : Neopixel LED

The first exercise was to display a color on  the RGB Neopixel LED built-in in the received module. There are two important things to know before we start coding in Arduino for this exercise. First is the fact that GP23 is the port where the built-in LED is plugged (It will be important to specify that in the code). Next is to understand a little bit the RGB system. It's a system that let you make any color with a base of red, green and blue. You can set the value of each of those color between 0 and 255. So for example :

|color|code|
|-----|----|
|Red|(255,0,0)|
| Green  | (0,255,0)  |
| Blue | (0,0,255) |
| Orange | (240,30,0) |

To start coding we also needed to install the [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) library on Arduino IDE. This library comes with a lot of examples on how to use the LED. So I started my code with one of those examples (called "simple") and modified what was needed to get what I wanted.

Here is my final code :

<img src="../images/mod4/led_code.png" width="500">

And here is the result on the RP2040 module :

<img src="../images/mod4/led.jpg" width="300" height="300">

After that you can easily do funner stuff such as automatically change the color every 10 seconds. Here is the code for that :

<img src="../images/mod4/led_code2.png" width="500">

As you can see, it is just a pretty easy addition of a delay time in between the display of different colors.

## Exo 2 : DHT20 - temperature & RH sensor

This next [exercise](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/exercices/dht20/) was to integrate the given (in the kit) sensor as input and get the result as an output. I managed to do the electronic circuit thanks to the [sensor datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) and this recapitulative table :

<img src="../images/mod4/sensor.png" width="500">

And this is the circuit :

<img src="../images/mod4/circuit_sensor.jpg" width="500">

Unfortunately, I worked with Theo for this part and we both encountered the same problem (and weren't able to solve it). After installing the needed [library](https://github.com/RobTillaart/DHT20) to use the sensor, we tried to upload our code on the MCU but then it disappeared and was no longer shown as connected to the USB port. We thus tried different possibilities including the one described in the "common problem" subsection that worked for a few students but not for us. So we couldn't finish the exercise.

This was the code that we tried (it is an exemple given in the DHT20 library) :

<img src="../images/mod4/dht.png" width="500">

You can look at [Camille's documentation](https://camille-lamon-fablab-ulb-enseignements-2023-2024-7a46eb513030b8.gitlab.io/fabzero-modules/module04/) to see what results we should have had.

## Exo 3 : Servo Motors

The last exercise was to use an analog knob to control the speed and direction of rotation of a [servo motor](https://www.tinytronics.nl/shop/en/mechanics-and-actuators/motors/servomotors/ts90d-mini-servo-1.6kg-continuous). An important part of this exercise was to understand the difference between analog and digital signal. Basically, analog signals are continuous waves that can vary in frequency and intensity while digital signal are represented in discrete values (such as a binary list). Here is a sketch representing that difference (found on the [Galen Carol Audio website](https://www.gcaudio.com/tips-tricks/digital-audio-cables-how-can-they-make-a-difference-by-nordost-audio/)) :

<img src="../images/mod4/digvana.jpg" width="500">

Here is the circuit for that exercise :

<img src="../images/mod4/servo.jpg" width="500">

and then, after downloading the adequate [library](https://www.arduino.cc/reference/en/libraries/servo/), I made that code that resulted in the "fan" of the motor turning in the direction of the knob and faster or slower depending on how far the knob was turned

<img src="../images/mod4/servo_code.png" width="400">

This code is based on the "Knob" example of the downloaded library.