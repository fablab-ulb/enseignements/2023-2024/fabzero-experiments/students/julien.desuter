# 2. Computer-Aided Design (CAD)

The goal of this week assignment was to design, model, and document a parametric FlexLinks construction kit that will be fabricated next week using 3D printers. It had to be parametric to allow adjustments accounting for the material properties and the machine characteristics.

We also had to properly license our work using a Creative Commons (CC) license and then use the code of someone else  in the class to adjust yours and combine both of the pieces together after printing them.

## My Flexlink

We were proposed and shown a couple of different programs that we could use to create our piece. I decided to use OpenSCAD as it is a program using code to create your model. The code is based on C which is a programming language that I learned last summer. Thus this was the more intuitive program for me compared to freeCAD where you need to use inside tools.

### OpenSCAD

Here is the OpenSCAD website (with a big button to download it) : [OpenSCAD](https://openscad.org/)

When you open OpenSCAD you get into the main page (see screenshot) where you can create a new project with the "New" button, open a previous project with "Open". The "Help" button will take you to the documentation page of the OpenSCAD website. Last but not least, you can open an example project in the given list under "Examples".

![](images/mod2/home_page.png)

Firstly, I decided to make an easy project just to familiarize myself with the software. So using commands and functions from the cheat sheet : [Cheat sheet](https://openscad.org/cheatsheet/index.html), I did a little stick with holes of different sizes to be able to test the printing machine. Here is what it looks like and the code that I used to do it ([Hole tester](files/hole_tester.scad))

![](images/mod2/hole_tester.png)

As you can see, it is indeed parametric (the parameters are defined above my function) so I can easily adapt them to my needs. Another important notice is the $fn=100 at the beginning of the code. It is the number of fragments and it define how your curves will look like. With a small fn, everything will look like squares while with a fn too big, the impression will be harder and take longer. It is thus fundamental to adjust it to the best with what you want to achieve. After a few testings, I decided that a hundred was the best fit for me.
Note that in C (or derivative), each line must end with a ";".

### Final piece

After seeing Edwin's piece (see last section), I decided to design a piece that could work with his. The goal was then to make a type of handle that could connect to two of his holes to hold his piece.

This is how I made it ([Handle](files/Handle.scad)):

![](images/mod2/handle.png)

Where h is the height of the handle and half the height of the studs, d is the diameter of the cylinders and l the length of the handle.
I used module to define the different parts of my object (for example with only one definition of stud I can call it twice and it will be the same if the arguments are the same but could also be different) It could be very useful for more complex pieces using lots of cylinders of different height or diameter.

The translate function helps you move away from the origin of the coordinate system while the hull function is a transformation that "displays the [convex hull](https://doc.cgal.org/latest/Convex_hull_2/index.html) of child nodes.


<p float="left">
  <img src="../images/mod2/cyl.png" width="250" />
  <img src="../images/mod2/hull.png" width="250" />
</p>

## Creative Commons Open-source License

"Creative Commons licenses give everyone from individual creators to large institutions a standardized way to grant the public permission to use their creative work under copyright law. From the reuser’s perspective, the presence of a Creative Commons license on a copyrighted work answers the question, What can I do with this work?"

There exist 6 different types of CC license that you can easily understand and choose from their website [creativecommons.org](https://creativecommons.org/share-your-work/cclicenses/).

As you can see at the top of my code,
```
// File : Handle.scad
// Author : Julien Desuter
// Date : 22/11/2023
// Licence : Creative Common Attribution Non-Commercial 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
``` 
I chose the simplest one, the CC BY license that basically allows everyone to use my work in anyway they want "as long as attribution is given to the creator" (in this case me). Here is the [CC BY 4.0 deed](https://creativecommons.org/licenses/by/4.0/).

## Edwin's piece

Here is Edwin's pieces with who we decided to combine our projects. Since I printed my piece after his, I had (already during the design) to adjust my parameters to fit with his. That's how I decided their value. The "eps" parameter in my piece is there to adjust the diameter of my cylinders so that after a few tries at the 3D printer it can fit with his piece.

![](images/mod2/edwin.png)

More information about his piece and thought process can be found in his documentation [Edwin's module 2](https://edwin-vanlanduyt-fablab-ulb-enseignements-2023-2-d1a3492ebb6b17.gitlab.io/fabzero-modules/module02/)

As you can also see, he chose a CC BY-NC license which means that one can use his work but not for commercial purpose and the one has to credit his work.