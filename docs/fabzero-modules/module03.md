# 3. 3D Printing

This week assignment was about 3D printing. We had to print our last week design and combine it with our partner one (Edwin in my case).
Here are my design files that I used for the 3D printing : 

- [.scad](files/Handle.scad)
- [.stl](files/Handle.stl)
- [g-code](Handle_0.3mm_PLA_MK3S_5m.gcode)

## PrusaSlicer

The first thing we had to do was to install a slicing software. I chose the recommended one [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903). This type of software allows you to convert STL files (or equivalents), that you can export from your project on OpenSCAD into g-code files containing specific print parameters.

### Configuration

But before one does that, one needs to configure the slicer. To do so, one firstly needs to select the printing machine. At the ULB FabLab, printing machines are **Original Prusa i3 MK3S/MK3S+ - 4mm Nozzle**. One can choose that in the configuration setups of PrucaSlicer as shown in the screenshot below

<img src="../images/mod3/config1.png" width="500">

Next, one needs to configure the setups that one wants on the top right corner of the PrusaSlicer main page (as shown below)

<img src="../images/mod3/config2.png" width="500">

Here, the printing setup (first line) helps one defines how precise one wants to be. Generic PLA is the material of the filament used while the most important parameter probably is the filling percentage (remplissage in French). Indeed, it will determine how strong the prototype will be but also directly relate to the time of the printing process. So if you set up a filling percentage very high the printing will take a long time (could be hours instead of minutes). Here I chose 15% as I thought it would be enough for the simplicity of my piece and so it would only take about 7 minutes. It indeed was enough since my piece is still intact.

### Exporting the g-code

Now that the setup is done, it is time to put the .stl file into the software. which gives me that :

<img src="../images/mod3/gcode1.png" width="500">

as you can see on the left of the screenshot there are quite a few different options to correctly place your piece so that it has as much contact with the plate as possible. Personally I had to turn it 180° so that the handle was the base and not the bottom of the cylinder (which would have resulted in printing the handle on nothing. Again seeing the simplicity of the piece it would probably not have change much but it's a good habit to take incase of a complexer piece).

then if you click on the bottom right button (not shown in the screenshot), you can slice your piece and show a preview of how it will be printed (slice by slice) but also the time that it will take (also for each slice).

<img src="../images/mod3/gcode2.png" width="500">

When everything is ready, you just need to re-push that same button (that became an export into g-code button) and put that g-code into an SD card that can go in the 3D printer.

## Printing

Now that the g-code is on the SD card it's time to put it on the machine. I, unfortunately, wasn't ready to do that part during the class hours so I had to take a time slot on [FabMan](https://fabman.io/members/1405/equipment) to be able to connect to the 3D printer on another day (just scan the QR code on the selected printer while connected to FabMan). 

Before anything, quickly clean the plate with the product available next to the printer. Then select your file on the SD card and launch the printing ! First the machine will heat the plane and then start printing.

Here is the machine while printing (left) and the result still on the plate (center) and the piece after I removed the printing residue (right) :

<p float="left">
  <img src="../images/mod3/printer.jpg" width="150" />
  <img src="../images/mod3/result1.jpg" width="200" />
  <img src="../images/mod3/result2.jpg" width="150" />
</p>

## Final piece

Last thing was to combine my piece with Edwin's one (see previous week documentation for more details on how we adjust our pieces together). Fortunately the parameter were directly the good ones and it fitted on first try. Here is the final result :

<p float="left">
<img src="../images/mod3/result3.jpg" width="200" />
<img src="../images/mod3/result4.jpg" width="200" />
</p>

You can also look at [Edwin's doc](https://edwin-vanlanduyt-fablab-ulb-enseignements-2023-2-d1a3492ebb6b17.gitlab.io/fabzero-modules/module03/) from this week for more details on his piece.