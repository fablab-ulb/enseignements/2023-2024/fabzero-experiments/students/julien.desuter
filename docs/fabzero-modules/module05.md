# 5. Team dynamics and final project

This last module was about the formation of groups for the final project. It's divided into 2 parts :

- The second one was to choose an object representing a problematic that you care about and create a group based on everyone's object. Then brainstorm to list problems that could be fitting in your group
- The last one was to develop 3 group dynamics tools and think about how you could implement them in your group

## Group formation and brainstorming on project problems

On this week class, we were asked to come with an object that represented a problematic. I chose a bottle cap. Lot's of people in the class thought it was there to represent plastic pollution but no ! I chose this item to represent handicap and more precisely the expensive costs that can come with it for materials or care. Indeed when I was in primary school, we had a girl in our class that needed a very sophisticated wheelchair. She had enough money so she had one but her parents still were in an association to help other children with the same condition but less fortunate. One of the action in this association was to collect bottle caps to recycle them in order to earn money and help pay for wheelchairs. So every year at my primary school we did a bottle cap collect. It's in that sense that I chose to bring a bottle cap for that exercise. 

Once we got to discuss a little bit with the others about their items, we had to make groups of 3 or 4 with people whose item intrigued us. I paired up with [Ali](https://ali-bahja-fablab-ulb-enseignements-2023-2024-fab-7873ad41e846a0.gitlab.io/) and [Camille](https://camille-lamon-fablab-ulb-enseignements-2023-2024-7a46eb513030b8.gitlab.io/) who respectively brought a syringe (Malaria in central Africa) and a medications tablet (Accessibility to medications). Since our 3 objects were related to the medical field the pairing seemed obvious.

### First brainstorming

After we formed the group and once again explained our problems to each other, we had to come up with as many words as possible related to our objects. For about 5 minutes we shouted as much as we could think of and Camille wrote them down. This was the result :

<img src="../images/mod5/words.png" width="500">

Then, from those words, we had to find problematics related to our items :

<img src="../images/mod5/probs.png" width="500">

We were on the same page here so it was pretty easy to find problematics and all of them where related to the medical field :

- How to improve healthcare accessibility ?
- How to improve social inequality in terms of health ?
- How to prevent common diseases ?
- How to raise awareness about disabilities ?

Then we were each given for post-it to vote for our favorite problematics. We could distribute the post-its as we wanted (all 4 on the same problematic or one on each or two an two and so on). But again we agreed and put all the 12 post-its on the same problematic : How to prevent common diseases ?

The last task of the day was to go to other groups to collect their opinions about our topics. And similarly other groups came to us so we gave them our opinion.

All of those exchange where pretty interesting and helped us better define our problematic. It's definitely an interesting way to form groups as it pushes towards getting to know and associating with new people.

## Group dynamics

### Rules Chart

First thing we decided as a group was to make a chart with common rules that were important to at least one person in the group and that everyone would have to respect. Those rules are :

- Actively participating in the discussion (everyone has to give his opinion before we decide)
- Adhering to deadlines (and don't work in the very last minute so if you need help you still have time to ask)
- Relative punctuality (15 minutes allowed)
- Honesty > Kindness (stay respectful though). It's important to say if you disagree
- Being responsive through messages (at least a little emoji to show that you care)
- Fair distribution of tasks (It's a group project so everyone has to work)

### Secretary and Chief

Next thing that we decided was to introduce a secretary and a chief for the group. Those roles will change every week in reverse order so that nobody does both roles on the same week. The chief's role is to decide deadlines for the week and make sure that everyone do their part until the next meeting. The secretary is the one that has to write the documentation of the week.

### Decision making

Last group dynamics tool we implemented was a way to take decision (in the case of a strong disagreement) it only ocurred once so far when we had to decide on which disease to work. we wrote all the possibilities and then each one gave 3 points to its favorite, 2 points to the second one and 1 point to the third. The remaining got no point. And then whatever has the most points wins

Unfortunately that ended up in a tie between Malaria and AIDS. So we ended up flipping a coin to decide. AIDS was the winner. The next week though we changed our minds and all of us agreed on Diabetes (that wasn't even a possibility on the first vote).