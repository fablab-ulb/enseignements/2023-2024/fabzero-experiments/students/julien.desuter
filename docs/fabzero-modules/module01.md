# 1. Project management and documentation

The goal of this assignment is to explore and use website development tools in order to elaborate a workflow for further projects.

## Introduction

Coming from Physics I already knew all the basic requirements to start this module. In fact, I already used git quite a few times, had VScode and was using it almost daily and had an SSH key. So I won't be able to go into details regarding the installation and setting up of those things since I did it several years ago but some of my colleagues explained all that in depth in their documentation.

Basically, I just had to find my public key in my .ssh hidden folder and put it in Gitlab. Then I copied my repository using the little button circled in red

![](images/mod1/clone.png)

and then clone it using the command line "git clone" followed by the copied link.

### Git push
An important note though is how to commit changes to your repository !
To do so, you need to enter the command lines on the following screenshot one by one :

![](images/mod1/push.png)

- git status : This command tells you what you have done since the last commit. It helps you recapitulate your work but is also very useful to make sure you're not committing anything that you don't want. If there are some files that you don't want to commit, you can use .gitignore but I would suggest reading the online doc on that since it is very clear.
Don't be afraid to use this command in between every other steps if you want to be sure of what you're actually doing.
- git add -A : is the command line to add all your directories and files to your repository. The -A tells it that you want "All". Another way to omit some of your files would be to manually add everything you want (instead of using -A) and so you just wouldn't add the ones you don't want.
- git commit -m "text" : is the command to commit your changes to the repository. The -m let's you write a message in between quotes "test" so you can remember what you have changed with this commit. It is very helpful in case you need to go back in time to a previous commit (for bug fixing for example).
- git push : simply pushes everything you committed to your repository.

Once you have pushed all your changes, it won't be updated online just yet. You have to wait for the next update on the pipeline schedule (or force it through a button)

### My workflow
My computer is on Windows but I use WSL (windows subsystem linux) which is a kind of virtual machine directly implemented in windows and really easy to install (see doc online) that gives you access to a Linux environment.

As I said, I have been using VScode for a long time now and my working space always look like that on it :

![](images/mod1/vscode.png)

On the left hand-side I can navigate through my folders and files. Then I split my screen in three parts. On the bottom is the terminal that is, as ones can see on the green box in the bottom left, on Ubuntu-20.04 using WSL. I am also using oh-my-zsh that gives the nice colors to it (helps with the clarity). Basically, it is an extension that you can easily find and download on your navigator.

The upper part is divided into 2 similar screens where I open my files. On the left I open the files in which I am currently working while the right is for the files in which I need to see something (here it's the preview of what I am writing on the left but it could also be a library in which I am using a function and need to see how the function is coded).

### project management principles

Since we have to do a documentation for a different assignment every week, the best way to handle this class would be to make sure that every thing is done (project and documentation) by the end of the week (before starting the new project).

Unfortunately, I did not really have a lot of time at the beginning of the semester (because of other classes). And so I fell behind a little bit. The important part for me to keep up was to make sure that I was done with the project before starting the new one (without the documentation).

So later down, when I came back for the documentation, I had everything done but needed to remember what I did a couple weeks earlier. So I worked on my doc using the spiral method presented during the first lecture (come back multiple times on the same thing to complete it):

- First : I made the structure of my docs while trying to remember whet I did + I thought and downloaded images that might be useful to illustrate what I wanted to say
- Second : Came back to my structure and wrote down everything in correct (and hopefully comprehensible) sentences.
- Third : After Edwin's review, I came back to modify what he suggested
- Fourth : I came back one more time because of problems with my images (they weren't appearing on the website correctly).

## Markdown

One new thing I learn during this first week was to use Markdown. according to the wikipedia page ([Markdown](https://en.wikipedia.org/wiki/Markdown)) it "is a lightweight markup language for creating formatted text using a plain-text editor. John Gruber created Markdown in 2004 as a markup language that is easy to read in its source code form.[9] Markdown is widely used for blogging and instant messaging, and also used elsewhere in online forums, collaborative software, documentation pages, and readme files".

It is indeed pretty easy and readable in its source code form because it is mainly plain text. I honestly just use a few syntaxes to add an image, link something or make a title. One can find all those syntaxes on the cheat sheet of the Markdown guide website : [Cheat sheet](https://www.markdownguide.org/cheat-sheet/).

## GraphicsMagick

The second thing I learned during this week is how to compress an image.
To do so, I downloaded GraphicsMagick using apt install (the sudo is just there to give access to the admin permissions of the computer).

![](images/mod1/install_gm.png)

Next I just used the convert -resize command (where the first file is the one you want to resize and the second one the new one that will be created).

![](images/mod1/gm.png)

At first I had a problem because gm was an Alias for git merge on my computer. So I had to find where that alias was and comment it out using a few bash commands such as :

- ls -la : shows all files and folder available where you are (even hidden ones).
- cd : gets you to a subfolder or to the previous one using "cd ..".
- grep : to see in which files I had "gm" written.
- vim : to open that file and modify it.

Other useful and very basic Bash commands are :

 - touch : to create a file.
 - rm : to remove a file.
 - mv : to move or rename a file.
 - mkdir : to create a new directory.

 I also installed GIMP which is an opensource version of photoshop. I haven't used it yet but might definitely be useful for me in the future since I already had to use photoshop a couple times for previous projects.
