// File : Handle.scad
// Author : Julien Desuter
// Date : 22/11/2023
// Licence : Creative Common Attribution Non-Commercial 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

$fn=100;

h=3;
d=6;
eps=0.1;
l=16;

module stud(height, diameter, epsilon){
    cylinder(h=height*2, d=diameter-epsilon);
}

module handle(height, diameter, epsilon, length){
    hull(){
        translate([0,0,height*2])
        cylinder(h=height, d=diameter-epsilon);
        
        translate([0,length,height*2])
        cylinder(h=height, d=diameter-epsilon);
    }
}

stud(h, d, eps);
translate([0,l,0])
stud(h, d, eps);

handle(h, d, eps, l);