$fn=100;

//Parameters
h = 1.8;
n = 5;
r = 2.4;
w = n + r;

module hole_tester(number, width, heigth, radius){
difference(){
    hull(){
        for (i = [0:number-1]){
            translate([width*i, 0, 0])
                cylinder(h=heigth, r=width/2, center=true);
            }
        }
    for (i = [0:number-1]){
        translate([width*i, 0, 0])
            cylinder(h=heigth+0.05, r=radius+(i*0.1), center=true);
        }
    }
};

hole_tester(n, w, h, r);